"""
TESTS
"""
import unittest
from chained_list import ChainedList

__author__ = "Samer El Khoury"

class TestLinkList(unittest.TestCase):
    """
    testing the methodes from chained_list
    """

    def setUp(self):
        self.test_list = ChainedList()

    def test_isempty(self): #pour creer un test le nom de la fonction commence par test_...
        """
        test to check that the list is empty
        """
        self.assertIsNone(self.test_list.first_node)

    def test_notempty(self):
        """
        test to check if the list is not empty
        """
        self.test_list.add_node(5)
        self.assertIsNotNone(self.test_list.first_node)

    def test_samelength(self):
        """
        test to check if the list's length is the same before and after adding then removing a node
        """
        first_length = self.test_list.get_count()
        self.test_list.delete_node(5)
        last_length = self.test_list.get_count()
        self.assertEqual(first_length, last_length)

    def test_sommet(self):
        """
        test to check if the last value is the bigger added value
        """
        self.test_list.add_node(30)
        self.test_list.add_node(15)
        last_value = self.test_list.get_lastnode()
        self.assertEqual(last_value, 30)


if __name__ == "__main__":
    unittest.main()
    
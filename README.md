# TP5 : Test logiciel

## ABOUT :
this program creates multiple tests to check if the chained list is empty, not empty...

## Installation :

To install this script use the following commands :

mkdir ~/Desktop/samerelkhoury/
cd ~/Desktop/samerelkhoury/
git clone https://gitlab.com/samerelkhoury/tp5.git

==> You're now good to go!

## COMMANDS TO RUN THE PROGRAM :
python3 pile_test.py (main file)

## Authors :
SAMER EL KHOURY


